<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/question', 'QuestionsController@index')->name('question');
Route::get('/examen', 'ReponseController@index')->name('examen');
Route::get('/module', 'ModuleController@index')->name('module');
Route::get('/passer_examen/{id}', 'PassExamenController@show')->name('passer_examen');
// Route::post('/reponseStudent/create', [App\Http\Controllers\QuestionsController::class, 'create'])->name('reponseStudent/create');
